using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBounce : MonoBehaviour
{
    PlayerManager player;
    private Rigidbody2D rb;
    Vector3 lastVelocity;
    private float speed;
    private float direction;

    // Start is called before the first frame update
    void Start()
    {
        rb.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Wall")) {
            lastVelocity = rb.velocity;
            speed = lastVelocity.magnitude;
            direction = Vector3.Dot(lastVelocity.normalized, (-transform.forward));
            rb.velocity = rb.transform.TransformDirection(direction * speed, 0, 0);
        }
    }
}
