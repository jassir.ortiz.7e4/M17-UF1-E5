using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] GameObject player;
    float posPlayerY;
    Vector3 posCamera;
    // Start is called before the first frame update
    void Start()
    {
        //posCamera = transform.position;
        posPlayerY = player.transform.position.y;
    }

    void Update()
    {
        if (player.transform.position.y > posPlayerY + 0.5f)
        {
            posPlayerY = player.transform.position.y;
            //posCamera = transform.position;
            GameManager.Instance.score = (int) posPlayerY;
        }
        //transform.position = new Vector3(posCamera.x, posPlayerY, posCamera.z);
        transform.position += Vector3.up * 1.5f * Time.deltaTime;
    }
}
