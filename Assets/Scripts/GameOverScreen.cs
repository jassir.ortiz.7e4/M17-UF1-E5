using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameOverScreen : MonoBehaviour
{
    public Text textScore;

    public void SetUp() {
        int maxScore;
        gameObject.SetActive(true);

        if (PlayerPrefs.GetInt("MaxScore") == 0)
        {
            PlayerPrefs.GetInt("MaxScore", 0);
        }
        else
        {
            maxScore = PlayerPrefs.GetInt("MaxScore");
            textScore.text = "MaxScore: " + maxScore.ToString();
        }
    }
    public void SetDisable() {
        gameObject.SetActive(false);
    }
    public void OnTextPlayAgain() {
        SceneManager.LoadScene("Game");
    }
}
