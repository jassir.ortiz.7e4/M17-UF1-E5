using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject heart1, heart2, heart3;
    public Text textScore;
    public GameOverScreen gameOverScreen;
    public int lifes = 2;
    public int score;
    public int maxScore;
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        Time.timeScale = 1;
        gameOverScreen.SetDisable();
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
    }

    private void Start()
    {
        heart1.SetActive(true);
        heart2.SetActive(true);
        heart3.SetActive(false);
    }

    void Update()
    {
        textScore.text = "Score: " + score.ToString();
        maxScore = score;
        IsDie();
    }

    void IsDie() {
        switch (lifes) {
            case 3:
                heart1.SetActive(true);
                heart2.SetActive(true);
                heart3.SetActive(true);
                break;
            case 2:
                heart1.SetActive(true);
                heart2.SetActive(true);
                heart3.SetActive(false);
                break;
            case 1:
                heart1.SetActive(true);
                heart2.SetActive(false);
                heart3.SetActive(false);
                break;
            case 0:
                heart1.SetActive(false);
                heart2.SetActive(false);
                heart3.SetActive(false);
                GameOver();
                break;
        }
    }

    void GameOver() {
        if (PlayerPrefs.GetInt("MaxScore") == null)
        {
            PlayerPrefs.SetInt("MaxScore", 0);
        }
        if (PlayerPrefs.GetInt("MaxScore") < maxScore) {
            PlayerPrefs.SetInt("MaxScore", maxScore);
            PlayerPrefs.Save();
        }
        gameOverScreen.SetUp();
        Time.timeScale = 0;
    }
}
