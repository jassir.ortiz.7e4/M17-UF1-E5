using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerManager: MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float maxHeight;
    [SerializeField] public float jumpHeight;
    [SerializeField] Animator animator;
    float numberJump;
    private Rigidbody2D rb;
    private bool isGrounded;    

    // Start is called before the first frame update
    void Start()
    { 
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        PlayerMovement();
    }

    void Jump() {
        if (numberJump > maxHeight - 1)
        {
            animator.SetBool("Land", true);
            numberJump = 0;
            isGrounded = false;
        }
        if(isGrounded){
            if (Input.GetKey(KeyCode.Space))
            {
                animator.SetBool("Idle", false);
                animator.SetBool("Jump", true);
                rb.velocity = Vector2.up * jumpHeight;
                numberJump += 1;
            }
        }
    }


    private void OnDestroy()
    {
        
    }

    void PlayerMovement() {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            animator.SetBool("Walk", true);
            animator.SetBool("Idle", false);
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            animator.SetBool("Walk", true);
            animator.SetBool("Idle", false);
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            GameManager.Instance.lifes--;
            Debug.Log(GameManager.Instance.lifes.ToString());
        }
        if (collision.gameObject.CompareTag("Fire"))
        {
            Debug.Log(GameManager.Instance.lifes.ToString());
            GameManager.Instance.lifes--;
            //SceneManager.LoadScene("Game");
        }
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
            animator.SetBool("Idle", true);
            animator.SetBool("Walk", false);
            animator.SetBool("Jump", false);
            animator.SetBool("Land", false);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
            animator.SetBool("Idle", false);
            animator.SetBool("Walk", false);
            animator.SetBool("Jump", false);
            animator.SetBool("Land", true);
        }
    }
}
