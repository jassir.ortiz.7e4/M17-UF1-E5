using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartController : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (GameManager.Instance.lifes != 3) {
            GameManager.Instance.lifes++;
        }
        Destroy(this.gameObject);
    }
}
