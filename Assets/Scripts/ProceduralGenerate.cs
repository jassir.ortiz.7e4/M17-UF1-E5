using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralGenerate : MonoBehaviour
{
    [SerializeField] Transform background;
    [SerializeField] Transform dungeonFrame;
    // Update is called once per frame
    void Update()
    {
        if (transform.position.y >= background.position.y + 10) {
            background.position = new Vector2(background.position.x, background.position.y + 10);
        }
        if (transform.position.y >= dungeonFrame.position.y + 10)
        {
            dungeonFrame.position = new Vector2(dungeonFrame.position.x, dungeonFrame.position.y + 10);
        }
    }
}
