using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject platform;
    [SerializeField] GameObject enemy;
    [SerializeField] GameObject heart;
    [SerializeField] GameObject player;
    Vector2 posPlaftform;
    float posPlayerY;
    void Start()
    {
        posPlaftform = transform.position;
        posPlayerY = player.transform.position.y;
        InvokeRepeating(nameof(spawnPlatform),1,1f);
    }

    private void FixedUpdate()
    {
        /*if (player.transform.position.y > posPlayerY + 1.2f)
        {
            posPlaftform = new Vector2(player.transform.position.x, transform.position.y);
            posPlayerY = player.transform.position.y;
            spawnPlatform();
        }*/
    }

    void spawnPlatform()
    {
        posPlaftform = new Vector2(player.transform.position.x, transform.position.y);
        posPlayerY = player.transform.position.y;
        GameObject platformObj = Instantiate(platform, new Vector3(Random.Range(player.transform.position.x, posPlaftform.x), posPlaftform.y, 0), Quaternion.identity);
        if (Random.Range(0, 8) == 2) {
            GameObject enemyObj = Instantiate(enemy, new Vector3(Random.Range(player.transform.position.x, posPlaftform.x), posPlaftform.y + 0.5f, 0), Quaternion.identity);
        }
        if (Random.Range(0, 16) == 2)
        {
            GameObject heartObj = Instantiate(heart, new Vector3(Random.Range(player.transform.position.x, posPlaftform.x), posPlaftform.y + 0.5f, 0), Quaternion.identity);
        }
    }
}
